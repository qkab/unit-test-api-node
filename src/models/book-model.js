import mongoose from 'mongoose';
import Joi from 'joi'
const Schema = mongoose.Schema;
const schema = Schema({
    title: { type: String, required: true },
    years: { type: Date, required: true, default: Date.now },
    pages: { type: Number, required: true }
});
schema.methods.joiValidate = function (request, schemaType) {
    const joiSchema = Joi.object().keys(schemaType);
    return Joi.validate(request, joiSchema);
}
export default mongoose.model('Book', schema, 'book');
