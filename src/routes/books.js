import fs from 'fs';
import path from 'path';
import uuid from 'uuid/v4';
import mongoose from 'mongoose';
import Book from '../models/book-model';
import bookValidator from '../utils/validations/book-validation';
import initIfFileIfMissing from '../utils/initIfFileMissing';
const checkDb = env => {
  let database;
  if (env === 'test') { database = process.env.MONGOURITEST; }
  if (env === 'dev') { database = process.env.MONGOURIDEV; }
  if (env === 'production') { database = process.env.MONGOURIPROD; }
  return database
}
const resetBooks = (req, res) => {
  let collectionToDelete = 'book';
  let database = checkDb(process.env.NODE_ENV);
  const connection = mongoose.createConnection(database, { useNewUrlParser: true });
  connection.on('open', async () => {
    if (await connection.db.listCollections({ name: collectionToDelete }).next()) {
      const collection = await connection.db.dropCollection(collectionToDelete);
      if (!collection) return res.status(400).send({ message: 'Error delete collection' });
    }
    if (typeof req.body.books !== 'object') return res.status(400).send({ message: 'Bad request' });
    await Promise.all(req.body.books.map(async book => {
      if (Object.keys(book).length === 3 && book.hasOwnProperty('title') && book.hasOwnProperty('years') && book.hasOwnProperty('pages')) {
        let newBook = new Book(book);
        let validation = newBook.joiValidate(book, bookValidator.create);
        if (validation.error) return res.status(400).send({ message: 'error adding the book' });
        console.log(validation);
        await newBook.save();
      }
    }));
    res.status(200).send({ message: 'Delete collection success', books: await Book.find() })
  });
}
const getBooks = async (req, res) => {
  try {
    const books = await Book.find();
    if (!books) return res.status(404).send({ message: 'error fetching books' })
    res.status(200).json({ books });
  } catch (error) {
    console.error(error)
  }
  // res.status(200).send({ message: 'route non activé' });

  // const pathBooks = path.join(__dirname, '../data/books.json');
  // fs.readFile(pathBooks, 'utf8', (err, data) => {
  //   if (err) {
  //     console.log(err);
  //     res.status(400).send({ message: 'error fetching books' });
  //   } else {
  //     console.log(data);
  //     res.status(200).send(JSON.parse(data));
  //   }
  // });
};

/*
 * POST /book to save a new book.
 */

const initialStructure = {
  books: []
};

const postBook = async (req, res) => {
  let book = new Book(req.body);
  let validation = book.joiValidate(req.body, bookValidator.create);
  if (validation.error) return res.status(400).send({ message: 'error adding the book' });
  await book.save();
  res.status(200).send({ message: 'book successfully added' });

  // res.status(200).send({ message: 'route non activé' });

  // const pathBooks = path.join(__dirname, '../data/books.json');
  // initIfFileIfMissing(pathBooks, initialStructure);
  // fs.readFile(pathBooks, 'utf8', (err, data) => {
  //   if (err) {
  //     console.log(err);
  //     res.status(400).send({ message: 'an Error occured' });
  //   } else {
  //     console.log(data);
  //     let obj;
  //     if (!data) {
  //       obj = obj = { books: [] };
  //     } else {
  //       obj = JSON.parse(data); //now it an object
  //     }
  //     if (!obj || !obj.books) obj = { books: [] };
  //     obj.books.push({
  //       id: uuid(),
  //       title: req.body.title,
  //       years: req.body.years,
  //       pages: req.body.pages
  //     }); //add some data
  //     const json = JSON.stringify(obj); //convert it back to json
  //     fs.writeFile(pathBooks, json, 'utf8', (err, data) => {
  //       if (err) {
  //         res.status(400).send({ message: 'error adding the book' });
  //       } else {
  //         res.status(200).send({ message: 'book successfully added' });
  //       }
  //     });
  //   }
  // });
};

/*
 * GET /book/:id route to retrieve a book given its id.
 */
const getBook = async (req, res) => {
  const book = await Book.findById(req.params.id);
  if (!book) return res.status(400).send({ message: 'book does not exist' });
  res.status(200).send({ message: 'book fetched', book });
  // res.status(200).send({ message: 'route non activé' });

  // const pathBooks = path.join(__dirname, '../data/books.json');
  // fs.readFile(pathBooks, 'utf8', (err, data) => {
  //   if (err) {
  //     console.log(err);
  //     res.status(400).send({ message: 'an Error occured' });
  //   } else {
  //     let obj = JSON.parse(data); //now it an object
  //     const book = obj.books.find(element => {
  //       if (element.id === req.params.id) return element;
  //     });
  //     if (!book) {
  //       return res.status(400).send({ message: 'book does not exist' });
  //     }
  //     console.log(req.params.id);
  //     console.log(book);
  //     res.status(200).send({ message: 'book fetched', book });
  //   }
  // });
};

/*
 * DELETE /book/:id to delete a book given its id.
 */
const deleteBook = async (req, res) => {
  const book = await Book.findById(req.params.id);
  if (!book) return res.status(400).send({ message: 'book does not exist' });
  let validation = book.joiValidate({ id: req.params.id }, bookValidator.delete);
  if (validation.error) return res.status(400).send({ message: 'error deleting the book' });
  const bookDeleted = await book.remove();
  if (!bookDeleted) return res.status(400).send({ message: 'error deleting the book' });
  res.status(200).send({ message: 'book successfully deleted' });
  // res.status(200).send({ message: 'route non activé' });

  // const pathBooks = path.join(__dirname, '../data/books.json');
  // fs.readFile(pathBooks, 'utf8', (err, data) => {
  //   if (err) {
  //     console.log(err);
  //     res.status(400).send({ message: 'an Error occured' });
  //   } else {
  //     let obj = JSON.parse(data); //now it an object
  //     const bookIndex = obj.books.findIndex(element => {
  //       if (element.id === req.params.id) {
  //         return element;
  //       }
  //     });
  //     console.log(bookIndex);
  //     if (bookIndex === -1) {
  //       return res.status(400).send({ message: 'book does not exist' });
  //     }
  //     obj.books.splice(bookIndex, 1);
  //     const json = JSON.stringify(obj);
  //     fs.writeFile(pathBooks, json, 'utf8', (err, data) => {
  //       if (err) {
  //         return res.status(400).send({ message: 'error deleting the book' });
  //       } else {
  //         return res.status(200).send({ message: 'book successfully deleted' });
  //       }
  //     });
  //   }
  // });
};

/*
 * PUT /book/:id to updatea a book given its id
 */
const updateBook = async (req, res) => {
  const book = await Book.findById(req.params.id);
  if (!book) return res.status(400).send({ message: 'book does not exist' });
  let validation = book.joiValidate({ id: req.params.id, title: req.body.title, years: req.body.years, pages: req.body.pages }, bookValidator.update);
  if (validation.error) return res.status(400).send({ message: 'error updating the book' });
  if (req.body.title) book.title = req.body.title;
  if (req.body.years) book.years = req.body.years;
  if (req.body.pages) book.pages = req.body.pages;
  const bookSaved = await book.save();
  if (!bookSaved) return res.status(400).send({ message: 'error updating the book' });
  res.status(200).send({ message: 'book successfully updated' });
  // res.status(200).send({ message: 'route non activé' });

  // const pathBooks = path.join(__dirname, '../data/books.json');
  // fs.readFile(pathBooks, 'utf8', (err, data) => {
  //   if (err) {
  //     console.log(err);
  //     res.status(400).send({ message: 'an Error occured' });
  //   } else {
  //     let obj = JSON.parse(data); //now it an object
  //     const bookIndex = obj.books.findIndex(element => {
  //       if (element.id === req.params.id) {
  //         return element;
  //       }
  //     });
  //     console.log(bookIndex);
  //     if (bookIndex === -1) {
  //       return res.status(400).send({ message: 'book does not exist' });
  //     }
  //     obj.books.splice(bookIndex, 1, {
  //       ...obj.books[bookIndex],
  //       title: req.body.title,
  //       years: req.body.years,
  //       pages: req.body.pages
  //     });
  //     const json = JSON.stringify(obj);
  //     fs.writeFile(pathBooks, json, 'utf8', (err, data) => {
  //       if (err) {
  //         return res.status(400).send({ message: 'error updating the book' });
  //       } else {
  //         return res.status(200).send({ message: 'book successfully updated' });
  //       }
  //     });
  //   }
  // });
};

//export all the functions
export default { getBooks, postBook, getBook, deleteBook, updateBook, resetBooks };
