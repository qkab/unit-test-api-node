import chai, { expect } from 'chai';
import chaiHttp from 'chai-http';
import chaiNock from 'chai-nock';
import chaiAsPromised from 'chai-as-promised';
import path from 'path';
import nock from 'nock';

import server from '../server';
import resetDatabase from '../utils/resetDatabase';

chai.use(chaiHttp);
const should = chai.should();
chai.use(chaiNock);
chai.use(chaiAsPromised);

// tout les packages et fonction nescessaire au test sont importé ici, bon courage
const apiUrl = "http://localhost:8080"


//passing this variable to integration to launch integration test
process.env.TEST_MODE = "integration"

//test unitaire

const resetDb = () => {
    return chai.request(apiUrl)
            .post('/reset')
            .send({ books: [{ title: "Coco raconte Channel 2", years: 1990, pages: 400 }] })
}


////////////////////////////////////////////////////BOOKS SUCCESS///////////////////////////////////////////

describe("# RESET DB", () => {
    it('should be work', done => {
        resetDb()
        .end((err, res) => {
            res.should.have.status(200)
            res.body.should.have.property("message")
            res.body.should.have.property("books")
            res.body.books.should.be.a('array')
            done()
        })
    })
})


describe('# BOOKS SUCCESS', () => {
    let defaultBook;
    let bookId;
    before((done) => {
        if (process.env.TEST_MODE !== "unit-test") {
            resetDb().end((err, res) => {
                defaultBook = res.body.books[0]
                bookId = defaultBook._id;
                done()
            })
        }else{
            defaultBook = { "id": "0db0b43e-dddb-47ad-9b4a-e5fe9ec7c2a9", "title": "Coco raconte Channel 2", "years": 1990, "pages": 400 }
            bookId = defaultBook.id;
            nock(apiUrl)
            .get('/book')
            .reply(200, {books: []})

            nock(apiUrl)
            .post('/book')
            .reply(200, {message: "book successfully added" })

            nock(apiUrl)
            .get(`/book/${bookId}`)
            .reply(200, {
                message: "book fetched",
                book: {
                    id: bookId,
                    title: "Coco raconte Channel 2",
                    years: 1990,
                    pages: 400
                }
            })

            nock(apiUrl)
            .put(`/book/${bookId}`)
            .reply(200, { message: 'book successfully updated' })

            nock(apiUrl)
            .delete(`/book/${bookId}`)
            .reply(200, { message: "book successfully deleted"})

            done()
        }
    })

    it('should get all books', (done) => {
        chai.request(apiUrl)
        .get('/book')
        .end((err, res) => {
            res.should.have.status(200)
            res.body.should.have.property('books')
            res.body.books.should.be.a("array")
            done()
        })
    })

    it('should create a new book', (done) => {
        chai.request(apiUrl)
        .post('/book')
        .send({title: "Oui-Oui contre Elizabeth II",years: 1990,pages: 400})
        .end((err, res) => {
            res.should.have.status(200);
            res.body.should.have.property("message");
            res.body.message.should.equal("book successfully added")
            done()
        })
    })

    describe("Book with id", () => {
        it('should get a book', (done) => {
            chai.request(apiUrl)
            .get(`/book/${bookId}`)
            .end((err, res) => {
                res.should.have.status(200)
                res.body.should.have.property('message')
                res.body.should.have.property('book')
                res.body.message.should.equal("book fetched")
                res.body.book.should.be.a("object")
                res.body.book.title.should.be.a("string")
                res.body.book.title.should.equal("Coco raconte Channel 2")
                done()
            })
        })
    
        it('should update a book', (done) => {
            chai.request(apiUrl)
            .put(`/book/${bookId}`)
            .send({title: "Oui- Oui contre Elizabeth II", years: 1990, pages: 400 })
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.have.property("message")
                res.body.message.should.equal('book successfully updated')
                done()
            })
        })
    
        it('should delete a book', (done) => {
            chai.request(apiUrl)
            .delete(`/book/${bookId}`)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.have.property("message")
                res.body.message.should.equal("book successfully deleted")
                done()
            })
        })
    })
})


////////////////////////////////////////////////////BOOKS ERRORS///////////////////////////////////////////

describe('# BOOKS FAILURE', () => {
    let defaultBook;
    let bookId;
    let fakeId = "5ccc14e60ffddd4de853ffe6"
    describe('Book with bad id', () => {
        before(() => {
            if (process.env.TEST_MODE === "unit-test") {
                nock(apiUrl)
                .get(`/book/${fakeId}`)
                .reply(400, { message: "book does not exist" })

                nock(apiUrl)
                .put(`/book/${fakeId}`)
                .reply(400, { message: "book does not exist" })

                nock(apiUrl)
                .delete(`/book/${fakeId}`)
                .reply(400, { message: "book does not exist"})
            }
        })

        it("should get a book but id is not valid", (done) => {
            chai.request(apiUrl)
                .get(`/book/${fakeId}`)
                .end((err, res) => {
                    res.should.have.status(400)
                    res.body.should.have.property('message')
                    res.body.message.should.equal("book does not exist")
                    done()
                })
        })


        it('should update a book but id is not valid', (done) => {
            chai.request(apiUrl)
                .put(`/book/${fakeId}`)
                .send({ title: "Oui- Oui contre Elizabeth II", years: 1990, pages: 400 })
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.have.property("message")
                    res.body.message.should.equal('book does not exist')
                    done()
                })
        })

        it('should delete a book but id is not valid', (done) => {
            chai.request(apiUrl)
            .delete(`/book/${fakeId}`)
            .end((err, res) => {
                res.should.have.status(400);
                res.body.should.have.property("message")
                res.body.message.should.equal("book does not exist")
                done()
            })
        })
    })

    describe("Books with error occured", () => {
        before(() => {
            defaultBook = { "id": "0db0b43e-dddb-47ad-9b4a-e5fe9ec7c2a9", "title": "Coco raconte Channel 2", "years": 1990, "pages": 400 }
            bookId = defaultBook.id;
            nock(apiUrl)
            .get('/book')
            .reply(400, { message: "error fetching books" })
    
            nock(apiUrl)
            .post('/book')
            .reply(400, { message: "error adding the book" })
    
            nock(apiUrl)
            .get(`/book/${bookId}`)
            .reply(400, { message: "an Error occured" })
    
            nock(apiUrl)
            .put(`/book/${bookId}`)
            .reply(400, { message: 'an Error occured' })
    
            nock(apiUrl)
            .delete(`/book/${bookId}`)
            .reply(400, { message: "an Error occured" })
        })
    
        it('should get all books but get error', (done) => {
            chai.request(apiUrl)
            .get('/book')
            .end((err, res) => {
                res.should.have.status(400)
                res.body.should.have.property("message")
                res.body.message.should.equal("error fetching books")
                done()
            })
        })
    
        it('should create a new book but get error', (done) => {
            chai.request(apiUrl)
            .post('/book')
            .send({ title: "Oui-Oui contre Elizabeth II", years: 1990, pages: 400 })
            .end((err, res) => {
                res.should.have.status(400);
                res.body.should.have.property("message");
                res.body.message.should.equal("error adding the book")
                done()
            })
        })
    
        describe("Book with id", () => {
            it('should get a book but get error', (done) => {
                chai.request(apiUrl)
                .get(`/book/${bookId}`)
                .end((err, res) => {
                    res.should.have.status(400)
                    res.body.should.have.property('message')
                    res.body.message.should.equal("an Error occured")
                    done()
                })
            })
    
    
            it('should update a book but get error', (done) => {
                chai.request(apiUrl)
                .put(`/book/${bookId}`)
                .send({ title: "Oui- Oui contre Elizabeth II", years: 1990, pages: 400 })
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.have.property("message")
                    res.body.message.should.equal('an Error occured')
                    done()
                })
            })
    
    
            it('should delete a book but get error', (done) => {
                chai.request(apiUrl)
                .delete(`/book/${bookId}`)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.have.property("message")
                    res.body.message.should.equal("an Error occured")
                    done()
                })
            })
        })
    })
})


// fait les Tests d'integration en premier




