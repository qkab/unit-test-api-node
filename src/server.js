import express from 'express';
import morgan from 'morgan';
import bodyParser from 'body-parser';
import dotEnv from 'dotenv';
import mongoose from 'mongoose';
import config from 'config';
import book from './routes/books';

let port = process.env.PORT || 8080;
let app = express();
let database;

dotEnv.config();
//don't show the log when it is test
if (config.util.getEnv('NODE_ENV') !== 'test') {
  //use morgan to log at command line
  app.use(morgan('combined')); //'combined' outputs the Apache style LOGs
}
if (process.env.NODE_ENV === 'test') database = process.env.MONGOURITEST;
if (process.env.NODE_ENV === 'dev') database = process.env.MONGOURIDEV;
if (process.env.NODE_ENV === 'production') database = process.env.MONGOURIPROD;

mongoose.Promise = global.Promise;
mongoose.set('debug', true);
mongoose.connect(database, { useNewUrlParser: true }).then(() => console.log('MongoDB connected')).catch(err => console.error(err))
//parse application/json and look for raw text
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.text());
app.use(bodyParser.json({ type: 'application/json' }));

app.get('/', (req, res) => res.json({ message: 'Welcome to our Bookstore!' }));

app
  .route('/book')
  .get(book.getBooks)
  .post(book.postBook);

app
  .route('/book/:id')
  .get(book.getBook)
  .delete(book.deleteBook)
  .put(book.updateBook);

app
  .route('/reset')
  .post(book.resetBooks)

app.listen(port);
console.log('Listening on port ' + port);

module.exports = app; // for testing
