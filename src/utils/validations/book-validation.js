import Joi from 'joi';
import joi_objectid from 'joi-objectid';

const Joi_id = joi_objectid(Joi);

export default {
    create: {
        title: Joi.string().regex(/^[a-zA-Z]/).required(),
        pages: Joi.number().required(),
        years: Joi.date().required(),
    },
    update: {
        id: Joi_id(),
        title: Joi.string().regex(/^[a-zA-Z]/),
        pages: Joi.number(),
        years: Joi.date()
    },
    delete: {
        id: Joi_id(),
    },
}